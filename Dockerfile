FROM ruby:2.5 as gembuilder

WORKDIR /build

# No need to moosh these up into a single RUN command for intermediate build
# stages.
RUN git clone https://github.com/strategicdata/csvlint.rb.git /build
RUN gem build csvlint.gemspec

#------------------------------------------------------------------------------

FROM ruby:2.5

WORKDIR /validator

COPY --from=gembuilder /build/csvlint-0.4.0.4.gem /validator/csvlint.gem

# Need LANG for utf8 support in csvlint
ENV LANG=C.UTF-8 \
    SCHEMA_URL=https://harvest.huni.net.au/api/huni-csv.json

# Install csvlint gem + dependencies
RUN gem install ffi \
    && gem install activesupport -v 5.2 \
    && gem install ./csvlint.gem

COPY ./docker-entrypoint.sh /validator/docker-entrypoint.sh

CMD ["bash"]
ENTRYPOINT ["/validator/docker-entrypoint.sh"]
