# HuNI CSV Feed format

# Format

The HuNI CSV Feed format is based on WC3's [CSV on the Web](https://w3c.github.io/csvw/) (CSVW) work. The schema, linked below,
supports a lowest common denominator and standardised supply of the [HuNI data model](https://bitbucket.org/huniteam/documentation/wiki/Data%20Model). 

## Validator

This provides a way to validate HuNI CSV Feed file format prior to uploading them for ingestion by the HuNI Harvester.

## Schema

The CSVW schema is available from https://harvest.huni.net.au/api/huni-csv.json .

Example csv files can be found at https://bitbucket.org/huniteam/huni-harvester/src/master/app/data/example-csv-files/ .

## Prerequisites

For macOS and Windows use [Docker Desktop](https://www.docker.com/products/docker-desktop).

For Linux; ensure Docker is installed as per your distribution's installation method.

### Usage

In the same directory as your HuNI CSV feed files run the following Docker command.

#### macOS & Linux

```shell
docker run --rm -v $(pwd):/csv-files -t huniteam/csv-validator:production
```

#### Windows

TODO - We assume it's the same as macOS and Linux but don't have an appropriate environment to test this on.
