#!/bin/bash

SCHEMA_FILENAME="huni-csv.json"

# Write the wget logs to wget.log, but only show them if there's a failure
# Curl is all or nothing in this version
if ! wget -nv -o wget.log -O $SCHEMA_FILENAME $SCHEMA_URL; then
  cat wget.log > /dev/stderr
  exit 1
fi

# Validate each file separately. If we do them all at once, it complains if any
# of them are missing. You also get a spurious encoding warning at the end if
# you do a batch validate.

for i in concept event organisation person place work; do
    if [[ -e /csv-files/$i.csv ]]
    then
        cp /csv-files/$i.csv .

        csvlint --schema=$SCHEMA_FILENAME $i.csv
    else
        echo Skipping $i.csv - file not found
    fi
done;

