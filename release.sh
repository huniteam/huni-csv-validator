#!/usr/bin/env bash

docker-compose build

docker tag huniteam/csv-validator huniteam/csv-validator:production

docker push huniteam/csv-validator:production
